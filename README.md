# Readme

This repo contains a minimal Python project which illustrates the Right Way TM
to package Python code.

See my article, [Python Packaging the Right Way](https://cooperativeengineering.org/posts/python-packaging-the-right-way/)
for more.
