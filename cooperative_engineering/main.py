#!/usr/bin/env python3

def tautology():
    """Always return ``True``."""
    return True

def main():
    """System entry point."""
    print('The rain in Spain falls mainly on the plain.')

if __name__ == '__main__':
    main()
