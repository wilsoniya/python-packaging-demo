#!/usr/bin/env python

from unittest import TestCase

from cooperative_engineering.main import tautology

class TestTautology(TestCase):
    def test_tautology(self):
        self.assertTrue( tautology())
