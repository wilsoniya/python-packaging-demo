#!/usr/bin/env python3

"""``cooperative_engineering`` is a package we're creating to demonstrate how
to properly package Python software.
"""

from setuptools import setup

setup(
    name='cooperative_engineering',
    version='0.1.0',
    long_description=__doc__,
    packages=['cooperative_engineering'],
    install_requires=[
        'leftpad==0.1.2',
    ],
    tests_require=['nose==1.3.7'],
    test_suite='nose.collector',
    entry_points={
        'console_scripts': [
            'run-ce=cooperative_engineering.main:main',
        ],
    },
)
